-- Snippet: readItems
readItems = do
    line <- getLine
    return $ map read $ words line

main = do
    [n, k] <- readItems
    let numOdd = div (n + 1) 2
    putStrLn $ show $ if k <= numOdd then k + k - 1 else (k - numOdd) * 2
