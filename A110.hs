main = do
    s <- getLine
    putStrLn $ if all is47 $ show $ length $ filter is47 s then "YES" else "NO"
    where is47 n = elem n "47"
