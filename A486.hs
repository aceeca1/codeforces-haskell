main = do
    n <- readLn
    putStrLn $ show $ if odd n then (-1) - (div n 2) else div n 2
