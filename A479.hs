main = do
    a <- readLn
    b <- readLn
    c <- readLn
    putStrLn $ show $ maximum [a + b + c, a + b * c, (a + b) * c,
                               a * b + c, a * b * c, a * (b + c)]
