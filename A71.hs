import Control.Monad

abbreviate s
    | length s <= 10 = s
    | otherwise = head s : show(length s - 2) ++ [last s]

main = do
    n <- readLn
    replicateM_ n $ do
        s <- getLine
        putStrLn $ abbreviate s
