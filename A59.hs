import Data.Char

main = do
    s <- getLine
    let shouldUpper = div (length s) 2 < length (filter isUpper s)
    putStrLn $ map (if shouldUpper then toUpper else toLower) s
