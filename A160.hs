import Data.List
import Data.Maybe

-- Snippet: readItems
readItems = do
    line <- getLine
    return $ map read $ words line

main = do
    n <- getLine
    a <- readItems
    let sums = scanr (+) 0 (sort a)
    let defective = (< 1 + div (sum a) 2)
    putStrLn $ show $ 1 + length a - fromJust (findIndex defective sums)
