-- Snippet: readItems
readItems = do
    line <- getLine
    return $ map read $ words line

main = do
    [n1, n2] <- readItems
    putStrLn $ if odd (min n1 n2) then "Akshat" else "Malvika"
