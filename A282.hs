import Control.Monad

main = do
    n <- readLn
    answer <- replicateM n $ do
        s1 : s2 : sn <- getLine
        return $ if s2 == '+' then 1 else -1
    putStrLn $ show $ sum answer
