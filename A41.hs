main = do
    s1 <- getLine
    s2 <- getLine
    putStrLn $ if reverse s1 == s2 then "YES" else "NO"
