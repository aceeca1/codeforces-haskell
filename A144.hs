import Data.List
import Data.Maybe

-- Snippet: readItems
readItems = do
    line <- getLine
    return $ map read $ words line

main = do
    n <- readLn
    a <- readItems :: IO [Int]
    let diffMin = fromJust $ findIndex (== (minimum a)) (reverse a)
    let diffMax = fromJust $ findIndex (== (maximum a)) a
    let diff = diffMin + diffMax
    putStrLn $ show $ if diff < n then diff else diff - 1
