-- Snippet: readItems
readItems = do
    line <- getLine
    return $ map read $ words line

main = do
    [n, m, a] <- readItems
    let divUp a1 a2 = div (a1 + a2 - 1) a2
    putStrLn $ show $ (divUp n a) * (divUp m a)
