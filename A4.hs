main = do
    n <- readLn
    putStrLn $ if even n && 4 <= n then "YES" else "NO"
