-- Snippet: readItems
readItems = do
    line <- getLine
    return $ map read $ words line

main = do
    [n, k] <- readItems
    a <- readItems
    let (aL, aR) = splitAt k a
    putStrLn $ show $ length $
        takeWhile (/= 0) (aL ++ takeWhile (== (last aL)) aR)
