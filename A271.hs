import Data.List
import Data.Maybe

main = do
    year <- readLn
    putStrLn $ show $ fromJust $ find distinctYear $ iterate (+ 1) (year + 1)
    where distinctYear n = (length $ nub $ show n) == 4
