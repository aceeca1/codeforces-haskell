import Data.List

-- Snippet: split
split c s
    | null s = [""]
    | c == (head s) = "" : r
    | otherwise = ((head s) : r1) : rn
    where r@(r1 : rn) = split c (tail s)

main = do
    s <- getLine
    putStrLn $ intercalate "+" $ sort $ split '+' s
