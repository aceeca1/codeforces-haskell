main = do
    n <- readLn
    putStrLn $ show $ div (n + 4) 5
