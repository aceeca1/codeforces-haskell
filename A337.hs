import Data.List

-- Snippet: readItems
readItems = do
    line <- getLine
    return $ map read $ words line

main = do
    [n, m] <- readItems
    a <- readItems
    putStrLn $ show $ minimum $ zipWith (-) (drop (n - 1) (sort a)) (sort a)
