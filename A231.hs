import Control.Monad

-- Snippet: readItems
readItems = do
    line <- getLine
    return $ map read $ words line

main = do
    n <- readLn
    answer <- replicateM n $ do
        s <- readItems
        return $ if sum s < 2 then 0 else 1
    putStrLn $ show $ sum answer
