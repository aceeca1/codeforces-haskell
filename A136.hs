import Data.Array.IArray

-- Snippet: readItems
readItems = do
    line <- getLine
    return $ map read $ words line

main = do
    n <- readLn
    a <- readItems
    let b = array (1, n) (zip a [1 ..])
    putStrLn $ unwords $ map show $ elems (b :: Array Int Int)
