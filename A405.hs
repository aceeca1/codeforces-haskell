import Data.List

-- Snippet: readItems
readItems = do
    line <- getLine
    return $ map read $ words line

main = do
    n <- getLine
    a <- readItems :: IO [Int]
    putStrLn $ unwords $ map show $ sort a
