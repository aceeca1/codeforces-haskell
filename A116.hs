import Control.Monad

-- Snippet: readItems
readItems = do
    line <- getLine
    return $ map read $ words line

main = do
    n <- readLn
    a <- replicateM n $ (readItems :: IO [Int])
    putStrLn $ show $ maximum (scanl process 0 a)
    where process people [exit, enter] = people - exit + enter
