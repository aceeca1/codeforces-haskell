import Data.Char

main = do
    s1 : sn <- getLine
    putStrLn $ toUpper s1 : sn
