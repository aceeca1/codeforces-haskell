isSubsequenceOf' s1 s2
    | null s1 = True
    | null s2 = False
    | head s1 == head s2 = isSubsequenceOf' (tail s1) (tail s2)
    | otherwise = isSubsequenceOf' s1 (tail s2)

main = do
    s <- getLine
    putStrLn $ if isSubsequenceOf' "hello" s then "YES" else "NO"
