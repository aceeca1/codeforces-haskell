import Control.Monad
import Data.List
import Data.Maybe

-- Snippet: readItems
readItems = do
    line <- getLine
    return $ map read $ words line

main = do
    a <- replicateM 5 $ do
        ai <- readItems
        return $ elemIndex 1 ai
    let i1 = fromJust (findIndex isJust a)
    let i2 = fromJust (a !! i1)
    putStrLn $ show $ abs (i1 - 2) + abs (i2 - 2)
