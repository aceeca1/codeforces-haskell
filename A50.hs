-- Snippet: readItems
readItems = do
    line <- getLine
    return $ map read $ words line

main = do
    [m, n] <- readItems
    putStrLn $ show $ div (m * n) 2
