-- Snippet: readItems
readItems = do
    line <- getLine
    return $ map read $ words line

main = do
    [k, n, w] <- readItems
    putStrLn $ show $ max 0 $ k * div (w * (w + 1)) 2 - n
