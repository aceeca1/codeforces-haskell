main = do
    s1 <- getLine
    s2 <- getLine
    putStrLn $ zipWith (\i1 i2 -> if i1 == i2 then '0' else '1') s1 s2
