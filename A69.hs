import Control.Monad
import Data.List

-- Snippet: readItems
readItems = do
    line <- getLine
    return $ map read $ words line

main = do
    n <- readLn
    a <- replicateM n $ readItems
    putStrLn $ if all (== 0) $ map sum $ transpose a then "YES" else "NO"
