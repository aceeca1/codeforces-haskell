import Control.Monad

main = do
    a <- replicateM 4 readLn
    d <- readLn
    let die n = any (\ai -> mod n ai == 0) a
    putStrLn $ show $ length $ filter die [1 .. d]
