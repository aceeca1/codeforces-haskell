main = do
    n <- readLn
    let answer = if even n then [4, n - 4] else [9, n - 9]
    putStrLn $ unwords $ map show answer
