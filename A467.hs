import Control.Monad

-- Snippet: readItems
readItems = do
    line <- getLine
    return $ map read $ words line

main = do
    n <- readLn
    a <- replicateM n $ do
        [living, total] <- readItems
        return $ if 2 <= total - living then 1 else 0
    putStrLn $ show $ sum $ a
