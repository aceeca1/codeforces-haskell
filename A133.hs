main = do
    s <- getLine
    putStrLn $ if any (\i -> elem i "HQ9") s then "YES" else "NO"
