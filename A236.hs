import Data.List

main = do
    s <- getLine
    putStrLn $ if odd $ length $ nub s then "IGNORE HIM!" else "CHAT WITH HER!"
