import Data.Array.IArray

-- Snippet: readItems
readItems = do
    line <- getLine
    return $ map read $ words line

main = do
    n <- getLine
    a <- readItems
    let b = accumArray (+) 0 (1, 4) [(i, 1) | i <- a]
    let [b1, b2, b3, b4] = elems (b :: Array Int Int)
    let answer1 = b4 + b3 + div (b2 + 1) 2
    let answer2 = div (3 + max 0 (b1 - (b3 + (mod b2 2) * 2))) 4
    putStrLn $ show $ answer1 + answer2
