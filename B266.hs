-- Snippet: readItems
readItems = do
    line <- getLine
    return $ map read $ words line

process s
    | null s = []
    | null $ tail s = s
    | s1 == 'B' && s2 == 'G' = 'G' : 'B' : process sn
    | otherwise = s1 : process (tail s)
    where s1 : s2 : sn = s

main = do
    [n, t] <- readItems
    s <- getLine
    putStrLn $ (iterate process s) !! t
