import Data.Char

main = do
    s <- getLine
    putStrLn [j | i <- map toLower s, not $ elem i "aeiouy", j <- ['.', i]]
