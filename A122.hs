lucky n = all (\i -> elem i "47") $ show n

-- Snippet: divisors
divisors1 i n
    | n == i * i = [i]
    | n <  i * i = []
    | mod n i == 0 = i : (div n i) : divisors1 (i + 1) n
    | otherwise = divisors1 (i + 1) n

divisors = divisors1 1

main = do
    n <- readLn
    putStrLn $ if any lucky $ divisors n then "YES" else "NO"
