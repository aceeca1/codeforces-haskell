import Data.List

-- Snippet: readItems
readItems = do
    line <- getLine
    return $ map read $ words line

main = do
    n <- readLn
    a1 <- readItems
    a2 <- readItems
    putStrLn $ if null ([1 .. n] \\ ((tail a1) ++ (tail a2)))
               then "I become the guy."
               else "Oh, my keyboard!"
