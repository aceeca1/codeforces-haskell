import Data.Char

main = do
    s <- getLine
    putStrLn $ if all isUpper (tail s) then map swapCase s else s
    where swapCase c = if isUpper c then toLower c else toUpper c
