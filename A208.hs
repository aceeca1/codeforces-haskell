-- Snippet: replace
replace s1 s2 s
    | null s = s
    | sTake == s1 = s2 ++ replace s1 s2 sDrop
    | otherwise = (head s) : replace s1 s2 (tail s)
    where (sTake, sDrop) = splitAt (length s1) s

main = do
    s <- getLine
    putStrLn $ unwords $ words $ replace "WUB" " " s
