import Data.Char

main = do
    s1 <- getLine
    s2 <- getLine
    putStrLn $ case compare (map toLower s1) (map toLower s2) of LT -> "-1"
                                                                 EQ -> "0"
                                                                 GT -> "1"
